<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use  App\Models\teacher;
use App\Http\Controllers\BaseController as BaseController;
use App\Http\Resources\teacher as teacherResource ;
use Validator;


class teacherController extends BaseController
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $teachers = teacher :: all();
       return $this->sendResponse (teacherResource::collection( $teachers),'all teacher sent')  ;
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $inpute = $request->all();
        $validitor = Validator ::make( $inpute ,[
        'firstname'=> 'required',
        'lastname'=> 'required',
        'phone'=> 'required',
        'classname'=> 'required',
        'subjectsname'=> 'required',
       
        ]);
        if ( $validitor->fails()){
            return $this->sendError('please validate error',$validitor-> errors());
        }
        $teacher = teacher:: create( $inpute);
        return $this->sendResponse (new teacherResource( $teacher),'teacher creat successfully');
        }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show()
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, teacher $teacher)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy(teacher $teacher)
    {
        //
    }
}